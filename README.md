# Quasar Transit Template (quasar-transit-template)

Quasar Transit Template\
This template contains an example on how to use eos-transit along with Scatter to login a user and transfer tokens

## Install the dependencies
```bash
yarn
```
### Important files

The boot folder contains the transit plugin that init the accessContext.\
It also triggers an autologin if there was previous successful login.

The store contains the account module where the login/logout are defined.\
The transfer module is an example on how to use eos-transit once the user is logged in.

### Env vars

Remember to rename `.env.example` to `.env` and set the different variables.


### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
yarn dev
```

### Lint the files
```bash
yarn run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
