export const setAccount = function (state, { account, walletId }) {
  if (!state.account) {
    localStorage.walletId = walletId
    state.account = account
    state.connecting = false
    this.$router.push({ path: '/transfer' })
  }
}

export const clearAccount = function (state) {
  localStorage.removeItem('walletId')
  state.account = null
  this.$router.push({ path: '/login' })
}

export const setConnecting = (state, connecting) => {
  state.connecting = connecting
}
